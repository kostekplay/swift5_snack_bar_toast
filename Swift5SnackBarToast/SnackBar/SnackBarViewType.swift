////  SnackBarView.swift
//  Swift5SnackBarToast
//
//  Created on 27/10/2020.
//  
//

import UIKit

class SnackBarView: UIView {

    let viewModel: SnackBarModel
    
    private var handler: Handler?
    
    private let label: UILabel = {
        let l = UILabel()
        l.textColor = .systemBackground
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    
    private lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    init(viewModel: SnackBarModel, frame: CGRect) {
        self.viewModel = viewModel
        super.init(frame: frame)
        
        addSubview(label)
        
        if viewModel.image != nil {
            addSubview(imageView)
        }
        
        backgroundColor = .label
        
        clipsToBounds = true
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        configure()
    }

    
    private func configure() {
        label.text = viewModel.text
        imageView.image = viewModel.image
        
        switch viewModel.type {
            case .action(let handler):
                self.handler = handler
                
                isUserInteractionEnabled = true
                let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapSnacBar))
                gesture.numberOfTouchesRequired = 1
                gesture.numberOfTapsRequired = 1
                addGestureRecognizer(gesture)
                
            case .info: break
        }
    }
    
    @objc private func didTapSnacBar(){
        handler?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if viewModel.image != nil {
            // label image
            imageView.frame = CGRect(x: 5, y: 5, width: frame.height - 10, height: frame.height - 10)
            label.frame = CGRect(x: imageView.frame.width + 5, y: 0, width: frame.size.width - imageView.frame.size.width - 5, height: frame.size.height)
        } else {
            label.frame = bounds
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
