////  ViewController.swift
//  Swift5SnackBarToast
//
//  Created on 27/10/2020.
//  
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for x in 0..<2 {
            let button = UIButton(frame: CGRect(x: 20,
                                                y: 130 + (CGFloat(x) * 55) + (CGFloat(x) * 10),
                                                width: view.frame.size.width - 40,
                                                height: 55))
            button.tag = x + 1
            button.backgroundColor = .systemBlue
            button.setTitle("Show SnackBar \(x + 1)", for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
            view.addSubview(button)
        }
    }
    
    @objc private func didTapButton(_ sender: UIButton) {
        
        let viewModel: SnackBarModel
        if sender.tag == 1 {
            viewModel = SnackBarModel(type: .info, text: "Hello bell", image: UIImage(systemName: "bell"))
        } else {
            viewModel = SnackBarModel(type: .action(handler: {[weak self] in
                DispatchQueue.main.async {
                    self?.showAlert()
                }
            }), text: "Alarm - alert", image: UIImage(systemName: "alarm"))
        }
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.size.width / 1.2, height: 60)
        let snackbar = SnackBarView(viewModel: viewModel, frame: frame)
        showSnacBar(snackBar: snackbar)
    }
    
    public func showSnacBar(snackBar: SnackBarView) {
        let width = view.frame.size.width/1.2
        
        snackBar.frame = CGRect(x: (view.frame.size.width - width)/2,
                                y: view.frame.size.height,
                                width: width,
                                height: 60)
        
        view.addSubview(snackBar)

        UIView.animate(withDuration: 0.3, animations: {
            snackBar.frame = CGRect(x: (self.view.frame.size.width - width)/2,
                                    y: self.view.frame.size.height - 130,
                                    width: width,
                                    height: 60)
        }, completion: { done in
            if done {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        snackBar.frame = CGRect(x: (self.view.frame.size.width - width)/2,
                                                y: self.view.frame.size.height,
                                            width: width,
                                            height: 60)
                    }, completion: { finished in
                        if finished {
                            snackBar.removeFromSuperview()
                        }
                    })
                })
            }
        })
    }
    
    private func showAlert(){
        let ac = UIAlertController(title: "It works", message: "You tapped message", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Dissmis", style: .cancel, handler: nil))
        present(ac, animated: true)
    }
}

